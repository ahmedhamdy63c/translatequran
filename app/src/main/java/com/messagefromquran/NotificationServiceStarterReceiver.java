package com.messagefromquran;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.messagefromquran.quranprojects.NotificationEventReceiver;

public class NotificationServiceStarterReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        NotificationEventReceiver.setupAlarm(context);
        NotificationEventReceiver.setupAlarmrandom(context,1);
    }
}
