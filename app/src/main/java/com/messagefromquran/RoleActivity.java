package com.messagefromquran;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.messagefromquran.quranprojects.NotificationEventReceiver;
import com.messagefromquran.quranprojects.activity.MainActivity;
import java.util.Random;

public class RoleActivity extends AppCompatActivity {
    private Button btn_browse,btn_notify;
    private ProgressDialog pd;

    static String lang;
    static String notify;
    SharedPreferences FirstRunPrefs = null;
    SharedPreferences dbVersionPrefs = null;
    SharedPreferences sharedPreferences;
    private int x;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_role);
        x=(int) Math.random();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        FirstRunPrefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        dbVersionPrefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);

        btn_browse=findViewById(R.id.btn_browse);
        btn_notify=findViewById(R.id.btn_notify);
        pd=new ProgressDialog(this);
        btn_browse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(RoleActivity.this, MainActivity.class);
                startActivity(i);
            }
        });


        btn_notify.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                pd.setMessage("you will be notified with ayah from quran within random time .....touch to cancel");
                pd.show();
                NotificationEventReceiver.setupAlarmrandom(getApplicationContext(),20);



            }
        });



    }

    @Override
    protected void onPause() {

        super.onPause();


    }

    @Override
    protected void onDestroy() {



        new Thread(new Runnable() {
            public void run() {

                NotificationEventReceiver.setupAlarmrandom(getApplicationContext(),x);

            }
        }).start();



        super.onDestroy();



    }
}
